<?php
/**
 * The template for the Homepage.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package understrap
 */

get_header();

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );
?>

<header class="hero component--hero home--hero">
	<?php
		if ( has_post_thumbnail() ) {
    echo '<div class="wallpaper" style="background-image:url( '.get_the_post_thumbnail_url().' );"></div>';
}

	else {
		echo '<div class="wallpaper" style="background-image:url(/wp-content/themes/bryans-wordpress-theme/images/bryans-on-4th-exterior.png);"></div>';
	}
 ?>
	<div class="container">
		<h1 class="component--hero-title">Downtown Waterloo's Finest Cuisine</h1>
		<div class="component--hero-actions">
			<!--<a href="#" class="btn btn-default btn-inverse">Learn More</a>-->

	</div>
	</div>
</header>

<div class="wrapper" id="page-wrapper">

	<div class="<?php echo esc_html( $container ); ?>" id="content" tabindex="-1">

		<div class="row">

			<main class="site-main" id="main">

				<section>
					<?php the_content(); ?>
				</section>

				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'loop-templates/content', 'page' ); ?>

				<?php endwhile; // end of the loop. ?>

			</main><!-- #main -->

		</div><!-- #primary -->


	</div><!-- .row -->

</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php echo do_shortcode("[wpv-view name='view-for-menus-events']"); ?>

<?php get_footer(); ?>
