<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

$the_theme = wp_get_theme();
$container = get_theme_mod( 'understrap_container_type' );
?>



<div class="wrapper" id="wrapper-contact-info">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<img src="/wp-content/themes/bryans-wordpress-theme/images/bryans_logo.svg" width="150" style="margin-bottom:15px;">
				<p>320 East 4th Street<br>
					Waterloo, IA 50703</p>
					<a href="https://www.google.com/maps/place/Bryan's+on+4th/@42.4993956,-92.3363475,17z/data=!3m1!4b1!4m5!3m4!1s0x87e552e51c1cc87f:0x3c91103de215f199!8m2!3d42.4993917!4d-92.3341588" target="_blank" class="btn btn-default">Get Directions</a>
			</div>
			<div class="col-md-4">
				<h3>Need a Reservation?</h3>
				<ul class="contact-info-list list-methods">
					<li><a href="/contact"><i class="fa fa-envelope-o" aria-hidden="true"></i> Email Us</a></li>
					<li><i class="fa fa-phone" aria-hidden="true"></i> (319) 274-8888</li>
				</ul>
			</div>
			<div class="col-md-4">
				<h3>Hours</h3>
					<?php echo do_shortcode("[mbhi_hours location='Bryan's on 4th']"); ?>
			</div>
		</div>
	</div>
</div>


<div class="wrapper" id="wrapper-footer">

	<div class="<?php echo esc_html( $container ); ?>">

		<div class="row">

			<div class="col-md-12">

				<footer class="site-footer" id="colophon" role="contentinfo">

					<div class="row">
						<div class="col-md-6 col-sm-12 site-info">
							&copy; <?php echo date("Y") ?> <?php bloginfo( 'name' ); ?>. All Rights Reserved.
						</div><!-- .site-info -->
						<div class="col-md-6 col-sm-12 social-media">
							<div class="social-media-links">
								<a href="https://www.facebook.com/Bryanson4th/" target="_blank" title="Visit our Facebook page"><i class="fa fa-facebook" aria-hidden="true"></i></a>
								<a href="https://www.youtube.com/channel/UCdVAYmv1NFzWNTUFOVp9xjw" target="_blank" title="Visit our YouTube channel"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
							</div>
						</div>
					</div>

				</footer><!-- #colophon -->

			</div><!--col end -->

		</div><!-- row end -->

	</div><!-- container end -->

</div><!-- wrapper end -->

</div><!-- #page -->

<?php wp_footer(); ?>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-89749718-1', 'auto');
  ga('send', 'pageview');

</script>
</body>

</html>
