<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package understrap
 */

get_header();
?>

<header class="hero component--hero home--hero">
<div class="wallpaper" style="background-image:url(/wp-content/themes/bryans-wordpress-theme/images/bryans-on-4th-exterior.png);"></div>
	<div class="container">
		<h1 class="component--hero-title">Page not found</h1>
	</div>
</header>

<div class="wrapper" id="page-wrapper">

	<div class="container" id="content">

		<div class="row">

			<div class="content-area" id="primary">

				<main class="site-main" id="main" role="main">

					<section class="error-404 not-found">



							<h2>We're sorry.</h2>


						<div class="page-content">

							<p>We couldn't find the page you were looking for.</p>
							<a href="/" class="btn btn-default">Return Home</a>


						</div><!-- .page-content -->

					</section><!-- .error-404 -->

				</main><!-- #main -->

			</div><!-- #primary -->

		</div> <!-- .row -->

	</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php echo do_shortcode("[wpv-view name='view-for-menus-events']"); ?>

<?php get_footer(); ?>
